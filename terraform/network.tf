# Network
resource "harvester_network" "net1" {
  name      = "net1"
  namespace = "default"

  vlan_id = 100

  route_dhcp_server_ip = ""
}

resource "harvester_network" "net2" {
  name      = "net2"
  namespace = "default"

  vlan_id = 200

  route_dhcp_server_ip = ""
}
