# https://github.com/harvester/harvester
# https://registry.terraform.io/providers/harvester/harvester/latest

terraform {
  required_providers {
    harvester = {
      source  = "harvester/harvester"
      version = "0.3.2"
    }
  }
}

provider "harvester" {
}
